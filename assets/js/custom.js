jQuery(document).ready(function() {
    // for hover dropdown menu
    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
    });

    jQuery(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $(".navbar-collapse").hasClass("collapse in");
        if( _opened === true && !clickover.hasClass("navbar-toggle") 
            && !clickover.parents(".header-container").length ){
            $("button.navbar-toggle").click();
        }

    });

    $("button.navbar-toggle").on('click', function (e) {
        if( $(".search-box:visible").length ){
            $(".search-box").slideUp();
        }
    });

    $(".search-icon").click(function(e) {
        $( ".search-box" ).slideToggle( "fast", function() {
            $(".search-txt").focus();
        });

        if( $(".navbar-collapse").hasClass("collapse in") ){
            $("#navbar").collapse('hide');
            $("button.navbar-toggle").addClass("collapsed");
        }
    });

    

    if( $('.main-page').length ){
        // slick slider call 
        $('.slick_slider').slick({
            dots: true,
            infinite: true,
            speed: 800,
            slidesToShow: 1,
            slide: 'div',
            autoplay: true,
            autoplaySpeed: 5000,
            cssEase: 'linear'
        });
    }

    if( $('.single-category-page').length ){
        
        $('#content').find(".article-card li").slice(0, 6).show();
        if( $(".article-card li:hidden").length == 0 ){
            $(this).closest('.single_post_content').find('.load-more').hide();
        }

        $(".load-more").on('click', function (e) {
            e.preventDefault();
            var content = $(this).closest('.single_post_content').find('li:hidden');
            content.slice(0, 1).slideDown();
            if( content.length == 0 ){
                $("#load").fadeOut('slow');
                $(this).closest('.single_post_content').find('.load-more').hide();
            }
        });
    }

    if( $('.category-page').length ){

        $(".article-card li").slice(0, 6).show();
        if( $(".article-card li:hidden").length == 0 ){
            $(this).closest('.single_post_content').find('.load-more').hide();
        }

        $(".load-more").on('click', function (e) {
            e.preventDefault();
            var content = $(this).closest('.single_post_content').find('li:hidden');
            content.slice(0, 1).slideDown();
            if( content.length == 0 ){
                $("#load").fadeOut('slow');
                $(this).closest('.single_post_content').find('.load-more').hide();
            }
        });

        $('.sub-menu a').on('click', function (e) {
            var tabname = $(this).attr('href');
            $(tabname).find("li").slice(0, 6).show();
            if( $(tabname).find("li:hidden").length == 6 ){
                $(tabname).find('.load-more').hide();
            }

            $(".load-more").on('click', function (e) {
                e.preventDefault();
                var content = $(tabname).find('li:hidden');
                content.slice(0, 1).slideDown();
                if( content.length == 0 ){
                    $("#load").fadeOut('slow');
                    $(tabname).find('.load-more').hide();
                }
            });
        });
    }

    $('#search-location').on('submit', function(e){
        e.preventDefault();

        var place   = $('input[name=place]').val();
        var city    = $('select[name=city-select]').val();
        var provice = $('select[name=provice-select]').val();

        if( !place ){
            $('input[name=place]').addClass('required');
        }
        if( !city ){
            $('select[name=city-select]').addClass('required');
        }
        if( !provice ){
            $('select[name=provice-select]').addClass('required');
        }

        if( place != '' && city != '' && provice != '' ){
            this.submit();
        }

    });

    $('select[name=city-select], select[name=provice-select]').on('change', function(e){
        $(this).removeClass('required');
    });
    
    $("input[name=place]").keypress(function( event ) {
        $(this).removeClass('required');
    });
    // jQuery('a.gallery').colorbox();
    //Check to see if the window is top if not then display button
    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });
    //Click event to scroll to top
    $('.scrollToTop').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    $('.tootlip').tooltip();

});